package co.simplon.promo18.springorm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Trainer {
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;
  private String name;
  private String city;


  @OneToMany(mappedBy = "trainerOfDog")
  @JsonIgnoreProperties("trainerOfDog")
  private List<Dog> trainedDogs = new ArrayList<>();

  public Trainer(Integer id, String name, String city) {
    this.id = id;
    this.name = name;
    this.city = city;
  }

  public Trainer(String name, String city) {
    this.name = name;
    this.city = city;
  }

  public Trainer() {
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public List<Dog> getTrainedDogs() {
    return trainedDogs;
  }

  public void setTrainedDogs(List<Dog> trainedDogs) {
    this.trainedDogs = trainedDogs;
  }
}
