package co.simplon.promo18.springorm.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Contest {

  @Id @GeneratedValue(strategy = GenerationType.IDENTITY) private Integer id;
  private LocalDate date;
  private String type;

  public Contest(Integer id, LocalDate date, String type) {
    this.id = id;
    this.date = date;
    this.type = type;
  }

  public Contest(LocalDate date, String type) {
    this.date = date;
    this.type = type;
  }

  public Contest() {
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }
}
