package co.simplon.promo18.springorm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Dog {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private Integer id;

  @NotBlank
  private String name;

  private String breed;

  @PastOrPresent
  private LocalDate birthdate;

  @JsonIgnoreProperties("trainedDogs")
  @ManyToOne(cascade = CascadeType.PERSIST)
  private Trainer trainerOfDog;


  @ManyToMany
  private List<Contest> contestsParticipated = new ArrayList<>();



  public Dog(Integer id, String name, String breed, @PastOrPresent LocalDate birthdate) {
    this.id = id;
    this.name = name;
    this.breed = breed;
    this.birthdate = birthdate;
  }

  public Dog(String name, String breed, @PastOrPresent LocalDate birthdate) {
    this.name = name;
    this.breed = breed;
    this.birthdate = birthdate;
  }

  public Dog() {
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBreed() {
    return breed;
  }

  public void setBreed(String breed) {
    this.breed = breed;
  }

  public LocalDate getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(LocalDate birthdate) {
    this.birthdate = birthdate;
  }
  public Trainer getTrainerOfDog() {
    return trainerOfDog;
  }

  public void setTrainerOfDog(Trainer trainerOfDog) {
    this.trainerOfDog = trainerOfDog;
  }

  public List<Contest> getContestsParticipated() {
    return contestsParticipated;
  }

  public void setContestsParticipated(List<Contest> contestsParticipated) {
    this.contestsParticipated = contestsParticipated;
  }
}
