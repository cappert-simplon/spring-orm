package co.simplon.promo18.springorm.controller;

import co.simplon.promo18.springorm.entity.Contest;
import co.simplon.promo18.springorm.entity.Dog;
import co.simplon.promo18.springorm.repository.ContestRepository;
import co.simplon.promo18.springorm.repository.DogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/dogs")
public class DogController {

  @Autowired private DogRepository dogRepo;
  @Autowired private ContestRepository contestRepo;

  @GetMapping
  public List<Dog> getAll(@RequestParam Optional<String> breed, @RequestParam Optional<String> search) {
    if (breed.isPresent()) {
      return dogRepo.findByBreed(breed.get());
    }
    if (search.isPresent()) {
      return dogRepo.findByBreedOrNameContaining(search.get(), search.get());
    }
    // else
    return dogRepo.findAll();
  }

  @GetMapping("/{id}")
  public Dog getOne(@PathVariable int id) {
    return dogRepo.findById(id)
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  @PatchMapping("{dogId}/contest/{contestId}")
  public Dog registerDogToContest(@PathVariable int dogId, @PathVariable int contestId) {
    // make Dog participate to contest
    Dog participatingDog = dogRepo.findById(dogId).get();
    // or use getOne() method in this Controller

    Contest contest = contestRepo.findById(contestId).get();

    participatingDog.getContestsParticipated().add(contest);

    return dogRepo.save(participatingDog);
  }
}
