package co.simplon.promo18.springorm.controller;

import co.simplon.promo18.springorm.entity.Contest;
import co.simplon.promo18.springorm.repository.ContestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

  @Autowired
  ContestRepository contestRepo;

  @GetMapping
  public List<Contest> all() {
    return this.contestRepo.findAll();
  }

  @PatchMapping // FIXME: PUT?
  public Contest save(@RequestBody Contest contest) {
    return this.contestRepo.save(contest);
  }
}
