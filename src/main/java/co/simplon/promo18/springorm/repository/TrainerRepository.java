package co.simplon.promo18.springorm.repository;

import co.simplon.promo18.springorm.entity.Dog;
import co.simplon.promo18.springorm.entity.Trainer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainerRepository extends JpaRepository<Trainer, Integer> {
}
