package co.simplon.promo18.springorm.repository;

import co.simplon.promo18.springorm.entity.Dog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DogRepository extends JpaRepository<Dog, Integer> {

  List<Dog> findByBreed(String breed);

  List<Dog> findByBreedOrNameContaining(String searchInBreed, String searchInName);
}
