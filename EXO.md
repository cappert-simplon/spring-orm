# Spring Boot avec JPA

1. Créer un projet spring boot qu'on va appeler springorm en lui mettant comme dépendances : spring devtools, mysql driver, spring data jpa et spring web
2. Dans ce projet, créer une entité Dog avec un id, un name, un breed et un birthdate
3. Créer une interface DogRepository et lui faire extends de JpaRepository<Dog,Integer> pour créer un repo complet avec tout le crud
4. Créer un contrôleur DogController et dedans faire un Autowired du DogRepository qu'on vient de "créer"
5. Dans ce contrôleur, utiliser les méthodes du DogRepository pour faire les différentes méthodes de l'API rest (donc un getAll sur /api/dog, un getById sur /api/dog/id, etc.) 

## Ajouter de la validation sur l'entité :
1. Rajouter dans le pom.xml (avec le add starters) le Validation I/O
2. Rajouter de la validation sur l'entité Dog, en considérant que le name est obligatoire, la birthdate doit être dans le passé
3. Faire en sorte que le post et le put soient validés 

## Relations one-to-many et many-to-many
Rajouter des dresseur·euses et des concours
1. Créer un entité Trainer qui aura un name, un id, une city
2. Créer une entité Contest qui aura un id, une date, un type (en string)
3. Dans le Trainer, rajouter une liste de Dog et dans le Dog rajouter un Trainer, puis chercher sur internet comment on fait un OneToMany avec Jpa (sachant que là ça sera OneToMany côté Trainer et ManyToOne côté Dog)
4. Dans le Dog, rajouter une List de Contest et dans le Contest une List de Dog puis refaire la même chose qu'avant mais cette fois ci pour un ManyToMany (des deux côtés) 

## Contest Contrôleur
1. Créer un ContestRepository et lui faire hériter de JpaRepository comme on a fait pour le DogRepository
2. Créer le ContestController dans lequel vous faites un autowired du repo
3. Créer une route GET /api/contest qui va renvoyer tous les contests et leurs chiens (il va falloir rajouter un JsonIgnoreProperties sur un des deux côtés de la relation manytomany)
4. Rajouter une route POST /api/contest qui va attendre un contest en requestbody et le faire persister avec le save
5. Côté DogController, rajouter une méthode PATCH qui va ressembler à /api/dog/id_du_chien/contest/id_du_contest et qui va donc avoir 2 pathvariable, se servir de ces deux id pour aller chercher le chien ainsi que le contest, puis ajouter le contest dans la la liste des contests du chiens et faire un save de ce dernier 

## Afficher les chiens par race et recherche
1. Dans le DogRepository, en vous basant sur la documentation de spring data (https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation), créer une méthode permettant de récupérer une liste de chien par leur breed
2. Dans le DogController, dans la méthode getAll, rajouter un RequestParam optionnel pour la breed, et s'il est présent, déclencher la méthode que vous venez de faire, sinon, déclencher le findAll classique
3. Rajouter une autre méthode dans le repository qui cette fois ci renverra une liste de chien en se basant sur la breed ou le name en utilisant un like, puis modifier la méthode getAll pour faire que si le RequestParam optionnel "search" est présent, alors on déclenche cette méthode du repo 
